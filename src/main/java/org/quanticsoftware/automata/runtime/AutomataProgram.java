package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.core.Concatenate;

public class AutomataProgram {
	private AutomataProgramContext programctx;
	
	public AutomataProgram(AutomataProgramContext programctx) {
		this.programctx = programctx;
	}
	
	public final ProgramItem[] items() {
		return programctx.items;
	}
	
	@Override
	public final String toString() {
		var sb = new StringBuilder();
		sb.append(Concatenate.execute("level: ", programctx.level, "\n"));
		sb.append(Concatenate.execute("id: ", programctx.id, "\n"));
		
		for (var item : programctx.items) {
			sb.append(Concatenate.execute(
					item.id, ": ",
					item.facility, ".",
					item.function,
					"\n"));
			
			for (var key : item.input.keySet())
				sb.append(Concatenate.execute(
						"-", key, ": ",
						item.input.get(key), "\n"));
		}
		
		return sb.toString();
	}
}
