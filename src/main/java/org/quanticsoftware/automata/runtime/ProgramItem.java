package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.Concatenate;

public class ProgramItem {
	public int id;
	public String facility, function;
	public Map<String, Input> input;
	
	public ProgramItem() {
		input = new HashMap<>();
	}
	
	public final void input(String name, String data) {
		input.put(name, new Input(data, null));
	}
	
	public final void input(String name, ProgramItem output) {
		input.put(name, new Input(Concatenate.execute("output.", output.id), null));
	}
	
	public final void input(String name, AutomataProgram program) {
		input.put(name, new Input(null, program));
	}
}

class Input {
	public AutomataProgram program;
	public String data;
	
	public Input(String data, AutomataProgram program) {
		this.data = data;
		this.program = program;
	}
	
	@Override
	public final String toString() {
		if (data != null)
			return data;
		if (program != null)
			return program.toString();
		return super.toString();
	}
}
