package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.core.DataObject;

public interface Source {
	
	public abstract DataObject get(String name);
	
}
