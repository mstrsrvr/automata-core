package org.quanticsoftware.automata.facilities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.DataType;

public class FunctionConfig {
	public Map<String, DataType> inputs;
	public Map<String, Set<Cauldron.property>> iconfig;
	public DataType output;
	public boolean allinput;
	public Set<Cauldron.property> properties;
	public Set<String> rejected;
	
	public FunctionConfig() {
		inputs = new HashMap<>();
		iconfig = new HashMap<>();
		properties = new HashSet<>();
		rejected = new HashSet<>();
	}
	
	@Override
	public final String toString() {
		return Concatenate.execute(
				"input:", inputs,
				", output:", output.toString(),
				", properties:", properties);
	}
}
