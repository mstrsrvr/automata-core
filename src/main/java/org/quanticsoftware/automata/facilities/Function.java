package org.quanticsoftware.automata.facilities;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;

public interface Function {
	
	public abstract void allinput(DataType type, FunctionRule rule);
	
	public abstract void bypass(DataType type, FunctionRule rule);
	
	public abstract void collect(DataType type, String field);
	
	public abstract FunctionConfig getConfig();
	
	public abstract void input(DataType type, String... name);
	
	public abstract void mtinput(DataType... types);
	
	public abstract void property(Cauldron.property property);
	
	public abstract void property(String name, Cauldron.property value);
	
	public abstract void reject(String function);
	
	public abstract FunctionRule rule();
	
	public abstract void rule(DataType output, FunctionRule rule);
	
	public abstract DataObject run(SharedContext shctx) throws Exception;
}
