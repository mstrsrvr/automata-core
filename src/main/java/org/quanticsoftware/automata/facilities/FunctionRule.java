package org.quanticsoftware.automata.facilities;

import org.quanticsoftware.automata.core.DataObject;

public interface FunctionRule {
	
	public abstract DataObject execute(Session session) throws Exception;
	
}
