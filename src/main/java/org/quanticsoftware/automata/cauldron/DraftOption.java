package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.Map;

public class DraftOption {
	public Map<String, Draft> input;
	public boolean finished;
	
	public DraftOption() {
		input = new HashMap<>();
	}
	
	public final String toString() {
		return input.toString();
	}
}

