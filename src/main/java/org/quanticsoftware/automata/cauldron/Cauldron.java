package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.Fail;
import org.quanticsoftware.automata.core.target.Target;
import org.quanticsoftware.automata.facilities.FunctionConfig;
import org.quanticsoftware.automata.runtime.AutomataProgram;
import org.quanticsoftware.automata.runtime.AutomataProgramContext;
import org.quanticsoftware.automata.runtime.ProgramItem;

public class Cauldron {
	
	public enum property {
		INPUT_PARAMETER_ONLY,
		CANT_STACK_SAME,
		CANT_UPDATE_INPUT,
		UPDATEABLE,
		CANT_ALLOW_INITIAL,
		RETURNS_INITIAL
	};
	
	private int toplevel, maxlevel;
	private Context context;
	private Target target;
	private Set<Integer> blacklist;
	private Span<Integer, String, OriginalFunction> fspan;
	private Span<String, DataType, Supplier> tspan;
	private Catalog catalog;
	
	public Cauldron(Context context, Target target) {
		this.context = context;
		this.target = target;
		blacklist = new HashSet<>();
		fspan = new Span<>();
		tspan = new Span<>();
	}
	
	private final void addFn2Tm(
			Map<String, CType> ctypes,
			OriginalFunction ofunction,
			Catalog catalog) {
		
		var template = getTemplate(ctypes, ofunction.config, catalog);
		
		var sb = new StringBuilder(ofunction.facility).
				append(".").
				append(ofunction.function);
		
		if (template.parameter)
			sb.append(":").append(ofunction.parameter);
		
		template.functions.put(sb.toString(), ofunction);
	}

	private final ProgramItem binstance(
			BuildContext buildctx,
			String facility,
			String function) {
		var fac = buildctx.programctx.context.getFacility(facility);
		if (fac == null)
			Fail.raise("%s is an invalid facility.", facility);
		
		if (fac.get(function) == null)
			Fail.raise("%s.%s is an invalid function.", facility, function);
		
		var item = new ProgramItem();
		item.id = buildctx.items.size();
		item.facility = facility;
		item.function = function;
		buildctx.items.add(item);
		return item;
	}
	
	private final ProgramItem build(
			BuildContext buildctx,
			LocalContext localctx) {
		var pitem = binstance(
				buildctx,
				localctx.supplier.facility,
				localctx.supplier.function);
		
		for (var ikey : localctx.input.keySet()) {
			var itemctx = localctx.input.get(ikey);
			
			var ditem = buildctx.pitems.get(itemctx.id);
			if (ditem == null) {
				ditem = build(buildctx, itemctx);
				buildctx.pitems.put(itemctx.id, ditem);
			}
			pitem.input(ikey, ditem);
		}
		
		if (localctx.supplier.parameter != null)
			pitem.input("input", localctx.supplier.parameter);
		
		buildctx.pitems.put(localctx.id, pitem);
		return pitem;
	}
	
	private final Draft dive(Catalog catalog, Draft parent, Supplier supplier) {
		
		if (parent.supplier != null) {
			if (isStackingSame(parent.supplier, supplier))
				return null;
			
			if (isFunctionRejected(parent.supplier, supplier))
				return null;
		}
		
		if (isInputParameterFailed(supplier))
			return null;
		
		var level = parent.level + 1;
		if (level > catalog.maxlevel)
			catalog.maxlevel = level;
		
		var draft = catalog.draft();
		draft.supplier = supplier;
		draft.parent = parent.id;
		draft.level = level;
		
		var hasparameter = draft.supplier.parameter != null;
		if (hasparameter || draft.supplier.config.inputs.isEmpty())
			return draft;
		
		draft.option = getDraftOptions(catalog, draft);
		if (draft.option == null)
			return null;
		
		return draft;
	}
	
	private final Draft dive(Catalog catalog, Supplier supplier) {
		
		if (isInputParameterFailed(supplier))
			return null;
		
		var draft = catalog.draft();
		draft.supplier = supplier;
		
		var hasparameter = supplier.parameter != null;
		if (hasparameter || supplier.config.inputs.isEmpty()) {
			if (isPointChecked(catalog, supplier.id))
				return null;
			
			catalog.resume.add(supplier.id);
			
			if (!catalog.suspended)
				catalog.last = supplier.id;
			
			catalog.suspended = true;
			return draft;
		}
		
		draft.option = getDraftOptions(catalog, draft);
		if (draft.option == null) {
			catalog.last = 0;
			catalog.suspended = false;
			return null;
		}

		catalog.suspended = true;
		return draft;
	}
	
	public final List<AutomataProgram> execute() {
		if (catalog == null) {
			catalog = getCatalog();
			if (catalog.groups == null)
				return null;
		}
		
		while (true) {
			var programs = make(catalog);
			
			if (programs != null) {
				if (!catalog.suspended)
					blacklist.add(toplevel++);
				return programs;
			}
			
			if ((maxlevel != 0) && (maxlevel < toplevel))
				return null;
			
			toplevel++;
		}
	}
	
	private final Catalog getCatalog() {
		var catalog = new Catalog();
		
		for (var key : target.input()) {
			var oname = target.input(key).name();
			var references = catalog.otyperefs.get(oname);
			if (references == null)
				references = catalog.otyperefs.instance(oname);
			
			var function = new String[] {
					"base",
					Concatenate.execute(oname, "_load")};
			
			var config = context.facility(function[0]).get(function[1]).getConfig();
			var ofunction = getOriginalFunction(function[0], function[1], config);
			ofunction.parameter = key;
			
			var ctypes = getSummaryConfig(config);
			addFn2Tm(ctypes, ofunction, catalog);
		}
		
		for (var key : context.facilities()) {
			var facility = context.getFacility(key);
			for (var fkey : facility.functions()) {
				var config = facility.get(fkey).getConfig();
				if (config.properties.contains(property.INPUT_PARAMETER_ONLY))
					continue;
				
				var ctypes = getSummaryConfig(config);
				addFn2Tm(ctypes, getOriginalFunction(key, fkey, config), catalog);
			}
		}
		
		var references = catalog.otyperefs.get(target.getOutputType().name());
		if (references != null)
			catalog.groups = getOrderedReferences(references);
		
		return catalog;
	}
	
	private final DraftOption getDraftOptions(Catalog catalog, Draft draft) {
		int nextlevel = draft.level + 1;
		if (nextlevel > toplevel)
			return null;
		
		var ooptions = getOptions(draft.supplier, catalog);
		if (ooptions == null)
			return null;
		
		var psize = draft.supplier.config.inputs.size();
		var input = new HashMap<String, Draft>();
		var seed = new Object[psize + 2];
		
		for (var ookey : ooptions.keySet()) {
			var ooption = ooptions.get(ookey);
			
			for (var oikey : ooption.keySet()) {
				var oitem = ooption.get(oikey);
				
				input.clear();
				int i = 0;
				for (var okey : oitem.keySet()) {
					var supplier = oitem.get(okey);
					if (isInitialReturn(draft.supplier, supplier, okey))
						break;
					
					var nextctx = dive(catalog, draft, supplier);
					if (nextctx == null)
						break;
					
					input.put(okey, nextctx);
					seed[i++] = nextctx.id;
				}
				
				if (input.size() != psize)
					continue;
				
				seed[i++] = nextlevel;
				seed[i] = draft.id;
				
				var checkpoint = Objects.hash(seed);
				if (isPointChecked(catalog, checkpoint))
					continue;
				
				catalog.resume.add(checkpoint);
				
				if (!catalog.suspended)
					catalog.last = checkpoint;
				
				for (var key : input.keySet()) {
					var nextctx = input.get(key);
					catalog.draftids.put(nextctx.id, nextctx);
				}
				
				var option = new DraftOption();
				option.input.putAll(input);
				
				return option;
			}
		}
		
		return null;
	}
	
	private final Map<Integer, Map<Integer, Map<String, Supplier>>> getOptions(
			Supplier supplier,
			Catalog catalog) {
		var name = supplier.toString();
		var ooptions = catalog.options.get(name);
		if (ooptions != null)
			return ooptions;
		
		var options = tspan.execute(
				supplier.config.inputs,
				t->catalog.otyperefs.get(t.name()));
		
		catalog.options.put(name, ooptions = getOrderedOptions(options));
		return ooptions;
	}
	
	private final Map<Integer, Map<Integer, Map<String, Supplier>>> getOrderedOptions(
			List<Map<String, Supplier>> options) {
		var groups = new TreeMap<Integer, Map<Integer, Map<String, Supplier>>>();
		int i = 0;
		
		for (var option : options) {
			var pn = 0;
			for (var okey : option.keySet()) {
				var ofunction = option.get(okey);
				if (ofunction.parameter == null)
					pn += ofunction.config.inputs.size();
			}
			
			var group = groups.computeIfAbsent(pn, k->new LinkedHashMap<>());
			group.put(i++, option);
		}
		
		return groups;
	}
	
	private final Map<Integer, List<Supplier>> getOrderedReferences(
			Map<String, Supplier> references) {
		
		var groups = new TreeMap<Integer, List<Supplier>>();
		for (var rkey : references.keySet()) {
			var supplier = references.get(rkey);
			var pn = 0;
			if (supplier.parameter == null)
				pn = supplier.config.inputs.size();
			
			var group = groups.computeIfAbsent(pn, p->new LinkedList<>());
			group.add(supplier);
		}
		
		return groups;
	}
	
	private final OriginalFunction getOriginalFunction(
			String facility,
			String function,
			FunctionConfig config) {
		var ctypes = new HashMap<String, Integer>();
		var ofunction = new OriginalFunction();
		ofunction.facility = facility;
		ofunction.function = function;
		ofunction.config = config;
		for (var key : config.inputs.keySet()) {
			var tname = config.inputs.get(key).name();
			var c = ctypes.containsKey(tname)? ctypes.get(tname) : 0;
			ofunction.input.put(Concatenate.execute(tname, ":", c), key);
			ctypes.put(tname, c + 1);
		}
		return ofunction;
	}
	
	private final Map<String, Supplier> getReferences(
			String oname,
			Catalog catalog) {
		var references = catalog.otyperefs.get(oname);
		if (references == null)
			catalog.otyperefs.put(oname, references = new HashMap<>());
		return references;
	}
	
	private final Map<String, CType> getSummaryConfig(FunctionConfig config) {
		var ctypes = new TreeMap<String, CType>();
		for (var ikey : config.inputs.keySet()) {
			var name = config.inputs.get(ikey).name();
			var ctype = ctypes.computeIfAbsent(name, k->new CType());
			ctype.c++;
			ctype.properties = config.iconfig.get(ikey);
		}
		
		return ctypes;
	}

	
	private final Template getTemplate(
			Map<String, CType> ctypes,
			FunctionConfig config,
			Catalog catalog) {
		var fname = getTemplateName(ctypes, config);
		var tname = Concatenate.execute("cauldron.", fname);
		var template = catalog.templates.get(tname);
		if (template != null)
			return template;
		
		catalog.templates.put(tname, template = new Template());
		template.function = fname;
		template.config = getTemplateConfig(ctypes, config);
		template.parameter = config.properties.contains(property.INPUT_PARAMETER_ONLY);
		
		var oname = template.config.output.name();
		var references = getReferences(oname, catalog);
		
		var supplier = supplier(
				"cauldron",
				template.function,
				template.config,
				template.parameter? "p" : null);
		
		references.put(supplier.name, supplier);
		
		return template;
	}
	
	private final FunctionConfig getTemplateConfig(
			Map<String, CType> ctypes,
			FunctionConfig config) {
		var tconfig = new FunctionConfig();
		var typectx = context.typectx();
		tconfig.output = config.output;
		for (var ikey : ctypes.keySet())
			for (int i = 0; i < ctypes.get(ikey).c; i++) {
				var name = Concatenate.execute(ikey, ":", i);
				tconfig.inputs.put(name, typectx.get(ikey));
				tconfig.iconfig.put(name, ctypes.get(ikey).properties);
			}
		
		tconfig.properties.addAll(config.properties);
		return tconfig;
	}
	
	private final String getTemplateName(
			Map<String, CType> ctypes,
			FunctionConfig config) {
		return Concatenate.execute(
				ctypes.toString(), ":",
				config.properties.toString(), ":",
				config.output.name());
	}
	
	public static final Cauldron instance(Context context, Target target) {
		return new Cauldron(context, target);
	}
	
	private final boolean isFunctionRejected(
			Supplier psupplier,
			Supplier supplier) {
		return psupplier.config.rejected.contains(supplier.name);
	}
	
	private final boolean isInitialReturn(
			Supplier psupplier,
			Supplier supplier,
			String item) {
		var retinitial = supplier.config.properties.contains(property.RETURNS_INITIAL);
		if (!retinitial)
			return false;
		
		var iconfig = psupplier.config.iconfig.get(item);
		if (iconfig == null)
			return false;
		
		return iconfig.contains(property.CANT_ALLOW_INITIAL);
	}
	
	private final boolean isInputParameterFailed(Supplier supplier) {
		if (!supplier.config.properties.contains(property.INPUT_PARAMETER_ONLY))
			return false;
		
		return (supplier.parameter == null);
	}
	
	private final boolean isPointChecked(
			Catalog catalog,
			int checkpoint) {
		
		if (catalog.suspended && catalog.resume.contains(checkpoint)) {
			if (catalog.last == checkpoint) {
				catalog.suspended = false;
				catalog.last = 0;
			}
			return true;
		}
		
		return false;
	}
	
	private final boolean isStackingSame(
			Supplier psupplier,
			Supplier lsupplier) {
		if (!lsupplier.name.equals(psupplier.name))
			return false;
		
		return lsupplier.config.properties.contains(property.CANT_STACK_SAME);
	}
	
	private final void link(Map<Integer, LinkContext> linkids, Catalog catalog) {
		for (var key : linkids.keySet()) {
			var draft = catalog.draftids.get(key);
			var linkctx = linkids.get(key);
			if (draft.parent > 0)
				linkctx.localctx.parentctx = linkids.get(draft.parent).localctx;
			
			if (linkctx.option == null)
				continue;
			
			for (var ikey : linkctx.option.input.keySet()) {
				var idraft = linkctx.option.input.get(ikey);
				var input = linkids.get(idraft.id).localctx;
				linkctx.localctx.input.put(ikey, input);
			}
		}
	}
	
	private final LocalContext localctx(
			Map<Integer, LinkContext> linkids,
			Draft draft) {
		var linkctx = new LinkContext();
		linkctx.localctx = new LocalContext();
		linkctx.localctx.id = draft.id;
		linkctx.localctx.input = new HashMap<>();
		linkctx.localctx.level = draft.level;
		linkctx.localctx.supplier = draft.supplier;
		linkctx.option = draft.option;
		
		linkids.put(linkctx.localctx.id, linkctx);
		return linkctx.localctx;
	}

	private final List<AutomataProgram> make(Catalog catalog) {
		
		for (var key : catalog.groups.keySet()) {
			var group = catalog.groups.get(key);
			
			for (var supplier : group) {
				if (catalog.suspended &&
						(catalog.first.supplier.id != supplier.id))
					continue;
				
				catalog.draftids.clear();
				catalog.dc = 0;
				catalog.maxlevel = 0;

				var draft = catalog.first = dive(catalog, supplier);
				if (draft == null)
					continue;
				
				if (blacklist.contains(catalog.maxlevel))
					continue;
				
				catalog.draftids.put(draft.id, draft);
				
				var models = split(catalog, draft);
				var programs = new LinkedList<AutomataProgram>();
				var buildctx = new BuildContext();
				
				for (var model : models) {
					var candidates = span1(catalog.templates, model);
					
					for (var candidate : candidates) {
						var stack = new Stack<LocalContext>();
						reorder(stack, candidate);
						
						buildctx.programctx = new AutomataProgramContext();
						buildctx.programctx.context = context;
						buildctx.programctx.id = Track.get(candidate);
						buildctx.programctx.level = toplevel;
						
						buildctx.items.clear();
						buildctx.pitems.clear();
						
						while (!stack.isEmpty())
							build(buildctx, stack.pop());
						
						buildctx.programctx.items = buildctx.items.toArray(
								new ProgramItem[0]);
						
						programs.add(new AutomataProgram(buildctx.programctx));
					}
				}
				
				return programs;
			}
		}
		
		return null;
	}
	
	private final void reorder(
			Stack<LocalContext> input,
			LocalContext parentctx) {
		input.push(parentctx);
		for (var key : parentctx.input.keySet())
			reorder(input, parentctx.input.get(key));
	}
	
	public final void setMaxLevel(int maxlevel) {
		this.maxlevel = maxlevel;
	}
	
	private final List<LocalContext> span1(
			Map<String, Template> templates,
			LocalContext localctx) {
		
		var output = span2(localctx);
		if (output.isEmpty())
			return List.of(localctx);
		
		var results = fspan.execute(output, n->templates.get(n).functions);
		var candidates = new LinkedList<LocalContext>();
		
		for (var result : results) {
			var candidate = span3(result, null, localctx);
			if (candidate != null)
				candidates.add(candidate);
		}
		
		return candidates;
	}
	
	private final void span2(Map<Integer, String> output, LocalContext localctx)
	{
		output.put(localctx.id, localctx.supplier.name);
		for (var key : localctx.input.keySet())
			span2(output, localctx.input.get(key));
	}

	private final Map<Integer, String> span2(LocalContext localctx) {
		var output = new HashMap<Integer, String>();
		span2(output, localctx);
		return output;
	}
	
	private final LocalContext span3(
			Map<Integer, OriginalFunction> template,
			LocalContext parentctx,
			LocalContext model) {
		
		var localctx = new LocalContext();
		localctx.id = model.id;
		localctx.parentctx = parentctx;
		localctx.input = new HashMap<>();

		var originalfnc = template.get(model.id);
		localctx.supplier = supplier(
				originalfnc.facility,
				originalfnc.function,
				originalfnc.config,
				originalfnc.parameter);
		
		var config = localctx.supplier.config;
		var cui = config.properties.contains(property.CANT_UPDATE_INPUT);
		
		for (var key : model.input.keySet()) {
			var candidate = span3(template, localctx, model.input.get(key));
			if (candidate == null)
				return null;
			
			var ikey = originalfnc.input.get(key);
			var iupd = config.iconfig.get(ikey).contains(property.UPDATEABLE);
			if (cui && iupd && (candidate.supplier.parameter != null))
				return null;
			
			localctx.input.put(ikey, candidate);
		}
		
		return localctx;
	}
	
	private final boolean split(List<Draft> draftl, int level, Draft draft)
			throws StartSplitBuildException{
		if (draft.option == null) {
			draftl.add(draft);
			return false;
		}
		
		if (draft.option.finished)
			return true;
		
		draftl.add(draft);
		
		for (var oikey : draft.option.input.keySet()) {
			var oidraft = draft.option.input.get(oikey);
			split(draftl, level + 1, oidraft);
		}
		
		draft.option.finished = true;
		
		if (level > 0)
			return true;
		
		throw new StartSplitBuildException();
	}
	
	private final List<LocalContext> split(Catalog catalog, Draft draft) {
		var models = new LinkedList<LocalContext>();
		var linkids = new HashMap<Integer, LinkContext>();
		
		if (draft.option == null) {
			models.add(localctx(linkids, draft));
			link(linkids, catalog);
			return models;
		}

		var draftl = new LinkedList<Draft>();
		while (true) {
			draftl.clear();
			try {
				if (split(draftl, 0, draft))
					break;
			} catch (StartSplitBuildException e) {
				splitbuild(models, linkids, catalog, draftl);
			}
		}
		
		return models;
	}
	
	private final void splitbuild(
			List<LocalContext> models,
			Map<Integer, LinkContext> linkids,
			Catalog catalog,
			List<Draft> draftl) {
		LocalContext sctx = null;
		for (var draft : draftl)
			if (sctx == null)
				models.add(sctx = localctx(linkids, draft));
			else
				localctx(linkids, draft);
		link(linkids, catalog);
	}
	
	private final Supplier supplier(
			String facility,
			String function,
			FunctionConfig config,
			String parameter) {
		var supplier = Supplier.instance();
		supplier.facility = facility;
		supplier.function = function;
		supplier.name = Concatenate.execute(facility, ".", function);
		supplier.config = config;
		supplier.parameter = parameter;
		
		return supplier;
	}
}

class CType {
	public int c;
	public Set<Cauldron.property> properties;
	
	@Override
	public final String toString() {
		if (properties.isEmpty())
			return Integer.toString(c);
		
		return Concatenate.execute(c, properties);
	}
}
