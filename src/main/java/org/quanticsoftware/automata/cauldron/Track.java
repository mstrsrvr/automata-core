package org.quanticsoftware.automata.cauldron;

public class Track {

	
	public static final String get(LocalContext localctx) {
		return get(localctx, true);
	}
	
	private static final String get(LocalContext localctx, boolean first) {
		if (first)
			while (localctx.parentctx != null)
				localctx = localctx.parentctx;

		var initialized = false;
		var sb = new StringBuilder(localctx.supplier.name).append("(");
		if (localctx.supplier.parameter != null) {
			sb.append(localctx.supplier.parameter);
			initialized = true;
		}
		
		for (var key : localctx.input.keySet()) {
			if (initialized)
				sb.append(",");
			else
				initialized = true;
			sb.append(get(localctx.input.get(key), false));
		}
		return sb.append(")").toString();
	}
}
