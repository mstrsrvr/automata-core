package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.facilities.FunctionConfig;

public class OriginalFunction {
	public String facility, function, parameter;
	public FunctionConfig config;
	public Map<String, String> input;
	
	public OriginalFunction() {
		input = new HashMap<>();
	}
	
	@Override
	public final String toString() {
		return Concatenate.execute(
				"facility:", facility,
				", function:", function,
				", config:", config,
				", input:", input.toString());
	}
}

