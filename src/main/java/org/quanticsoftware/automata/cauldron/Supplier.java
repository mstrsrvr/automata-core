package org.quanticsoftware.automata.cauldron;

import org.quanticsoftware.automata.facilities.FunctionConfig;

public class Supplier {
	private static int counter;
	public int id;
	public String facility, function, name, parameter;
	public FunctionConfig config;
	
	public static final Supplier instance() {
		var supplier = new Supplier();
		supplier.id = counter++;
		return supplier;
	}
	
	@Override
	public final String toString() {
		return name;
	}
}
