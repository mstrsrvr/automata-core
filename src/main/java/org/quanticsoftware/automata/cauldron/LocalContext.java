package org.quanticsoftware.automata.cauldron;

import java.util.Map;

import org.quanticsoftware.automata.core.Concatenate;

public class LocalContext {
	public int id, level;
	public Supplier supplier;
	public Map<String, LocalContext> input;
	public LocalContext parentctx;
	
	@Override
	public final String toString() {
		return Concatenate.execute(id, ":", Track.get(this));
	}
}
