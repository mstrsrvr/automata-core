package org.quanticsoftware.automata.core.target;

import org.quanticsoftware.automata.core.DataObject;

public interface OutputTest {
	
	public abstract boolean test(DataObject object);
	
}
