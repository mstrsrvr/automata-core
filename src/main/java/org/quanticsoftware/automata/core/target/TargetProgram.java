package org.quanticsoftware.automata.core.target;

public interface TargetProgram {
	
	public abstract void execute(TargetProgramAPI api);
	
}
