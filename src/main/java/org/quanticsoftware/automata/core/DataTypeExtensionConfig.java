package org.quanticsoftware.automata.core;

public interface DataTypeExtensionConfig {
	
	public abstract void execute(DataTypeExtension extension);
	
}
