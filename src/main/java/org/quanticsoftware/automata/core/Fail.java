package org.quanticsoftware.automata.core;

public class Fail {
	
	public static final void raise(String text, Object... args) {
		throw new GeneralException(text, args);
	}
}
